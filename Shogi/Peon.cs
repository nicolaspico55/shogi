﻿using System.Drawing;

namespace Shogi
{
    public class Peon : Pieza
    {
        public string Color { get; set; }
        public override bool ValidarMovimiento(Point posicionInicial, Point posicionFinal)
        {
            return (posicionInicial.X == posicionFinal.X || posicionInicial.Y == posicionFinal.Y);
        }
    }
}
