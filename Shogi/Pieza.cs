﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Shogi
{
    public abstract class Pieza
    {
        public abstract bool ValidarMovimiento(Point posicionInicial, Point posicionFinal);
    }
}
