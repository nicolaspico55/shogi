﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Shogi
{
    public class Tablero
    {
        public Pieza[,] posiciones;

        public Tablero()
        {
            posiciones = new Pieza[9, 9];
        }

        public void AgregarPieza(Peon peon, Point posicion)
        {
            posiciones[posicion.X, posicion.Y] = peon;
        }

        public void Mover(Point posicionInicial, Point posicionFinal)
        {
            var piezaMover = posiciones[posicionInicial.X, posicionInicial.Y];
            if (piezaMover != null)
            {
                if (piezaMover.ValidarMovimiento(posicionInicial, posicionFinal))
                {
                    posiciones[posicionFinal.X, posicionFinal.Y] = piezaMover;
                    posiciones[posicionInicial.X, posicionInicial.Y] = null;

                }
            }
        }
    }
}
