using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shogi;

namespace ShogiTest
{
    [TestClass]
    public class TableroTest
    {
        [TestMethod]
        public void SeCreaUnTableroDe9x9()
        {
            //Arrange
            var tablero = new Tablero();

            //Assert
            Assert.AreEqual(tablero.posiciones.GetLength(0), 9);
            Assert.AreEqual(tablero.posiciones.GetLength(1), 9);

        }

        [TestMethod]
        public void AgregarPeonEnPosicion()
        {
            //Arrange
            var tablero = new Tablero();
            var peon = new Peon();
            peon.Color = "Blanco";
            Point posicion = new Point(0, 6);

            //Act
            tablero.AgregarPieza(peon, posicion);

            //Assert
            Assert.AreEqual(tablero.posiciones[0, 6], peon);

        }

        [TestMethod]
        public void MoverPeonCorrectamente()
        {
            //Arrange
            var tablero = new Tablero();
            var peon = new Peon();
            peon.Color = "Blanco";
            Point posicionInicial = new Point(0, 6);
            Point posicionFinal = new Point(0, 5);

            //Act
            tablero.AgregarPieza(peon, posicionInicial);
            tablero.Mover(posicionInicial, posicionFinal);

            //Assert
            Assert.AreEqual(tablero.posiciones[0, 6], null);
            Assert.AreEqual(tablero.posiciones[0, 5], peon);

        }

        [TestMethod]
        public void MoverPeonIncorrectamente()
        {
            //Arrange
            var tablero = new Tablero();
            var peon = new Peon();
            peon.Color = "Blanco";
            Point posicionInicial = new Point(0, 6);
            Point posicionFinal = new Point(1, 5);

            //Act
            tablero.AgregarPieza(peon, posicionInicial);
            tablero.Mover(posicionInicial, posicionFinal);

            //Assert
            Assert.AreEqual(tablero.posiciones[0, 6], peon);
            Assert.AreEqual(tablero.posiciones[1, 5], null);
        }

        [TestMethod]
        public void MoverPeonPosicionVacioIncorrectamente()
        {
            //Arrange
            var tablero = new Tablero();
            var peon = new Peon();
            peon.Color = "Blanco";
            Point posicionInicial = new Point(0, 6);
            Point posicionFinal = new Point(1, 5);

            //Act
            tablero.AgregarPieza(peon, posicionFinal);
            tablero.Mover(posicionInicial, posicionFinal);

            //Assert
            Assert.AreEqual(tablero.posiciones[0, 6], null);
            Assert.AreEqual(tablero.posiciones[1, 5], peon);
        }
    }


}
